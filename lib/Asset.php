<?php namespace SteeveDroz\CiAsset;

/**
 * This library allows an easy use of assets in CodeIngiter 4.
 *
 * It is supposed to be used:
 * <ol>
 * <li>In a controller, by calling <code>addCss()</code> or <code>addJs()</code>.</li>
 * <li>In a corresponding view, by calling <code>css()</code> or <code>js()</code>.</li>
 * </ol>
 *
 * It is also possible to use it as a service.
 */
class Asset
{
    /**
     * Everything about stylesheets with entries corresponding to <code>sheet => media</code>.
     */
    protected $css = [];
    /**
     * Everything about scripts with entries corresponding to <code>script => defer</code>.
     */
    protected $js = [];

    /**
     * This method allows to add CSS to the asset.
     *
     * @param string $file The file to load, can either be a name such as "menu", in which case it represents "/css/menu.css", or an absolute path such as "https://example.com/menu.min.css", in which case it represents the given location.
     * @param string $media The type of media. By default, the value is "all", but it can be anything, as simple as "print" or as complicated as "screen and (min-width: 30em) and (orientation: landscape)".
     * @return Asset itself, for method chaining
     */
    public function addCss(string $file, string $media = 'all'): Asset
    {
        $this->css[$file] = $media;
        return $this;
    }

    /**
     * This method allows to add JS to the asset.
     *
     * @param string $file The file to load, can either be a name such as "animation", in which case it represents "/js/animation.css", or an absolute path such as "https://example.com/animation.min.js", in which case it represents the given location.
     * @param bool $defer The type of media. By default, the value is false, which means the script will be loaded right away. When true, it will wait for the DOM to be ready.
     * @return Asset itself, for method chaining
     */
    public function addJs($file, bool $defer = false): Asset
    {
        $this->js[$file] = $defer;
        return $this;
    }

    /**
     * Returns the CSS already loaded.
     * @return array A copy of the current CSS array.
     */
    public function getCss(): array
    {
        return $css = $this->css;
    }

    /**
    * Returns the JS already loaded.
    * @return array A copy of the current JS array.
    */
    public function getJs()
    {
        return $js = $this->js;
    }

    /**
     * Returns the code to insert into <code>&lt;head&gt;</code> in order to load all the CSS sheets.
     * @return string A string containing link tags separated by new lines.
     */
    public function css(): string
    {
        $css = $this->getCss();
        $lines = [];
        foreach ($css as $name => $media)
        {
            if (!preg_match('#^\w+://#', $name))
            {
                $name = '/css/' . $name . '.css';
            }

            $lines[] = '<link rel="stylesheet" href="' . $name . '" type="text/css" media="' . $media . '">';
        }

        return implode(PHP_EOL, $lines);
    }

    /**
     * Returns the code to insert into <code>&lt;head&gt;</code> in order to load all the JS scripts.
     * @return string A string containing script tags separated by new lines.
     */
    public function js()
    {
        $js = $this->getJs();
        $lines = [];
        foreach ($js as $name => $defer)
        {
            if (!preg_match('#^\w+://#', $name))
            {
                $name = '/js/' . $name . '.js';
            }
            $lines[] = '<script src="' . $name . '"' . ($defer ? ' defer' : '') . '></script>';
        }

        return implode(PHP_EOL, $lines);
    }

    /**
     * A convinient way to use CSS with services.
     *
     * This requires a service called "asset" to be instanciated.
     */
    public static function staticCss(): string
    {
        return service('asset')->css();
    }

    /**
     * A convinient way to use JS with services.
     *
     * This requires a service called "asset" to be instanciated.
     */
    public static function staticJs(): string
    {
        return service('asset')->js();
    }
}
